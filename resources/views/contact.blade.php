@extends('layouts.app')

@section('content')
    <h3>Contact Form - Laravel</h3>
    <form action="{{ route('sender') }}" method="post">
        @csrf
        <label class="form-inline" for="name">Name:</label>
        <input class="form-inline" id="name" name="fname" type="text"/>
        <label class="form-inline" for="email">Email:</label>
        <input class="form-inline" id="email" name="femail" type="email" required/>
        <label class="form-inline" for="message">Texto:</label>
        <textarea class="form-control" id="message" rows="6" name="fmessage" size="500" maxlength="500" placeholder="Message text..." required></textarea>
        <br/>
        <input type="submit" value="send">
        <input type="reset" value="reset">
    </form>
@endsection