@extends('layouts.app')

@section('content')
    <h3>All Posts in DB - Laravel</h3>
    <hr/>
    @if(count($posts)==0)
        <p>Todavia no se ha escrito nada. Sé el primero en escribir una entrada.</p>
    @else
        @foreach($posts as $post)
            <section>
                <h3>{{$post['title']}}</h3>
                <p>{{$post['article']}}</p>
                <br>
                <a type="button" class="btn btn-info" href="{{route('preview',['title'=>$post['title'], 'text'=>$post['article']])}}">Preview</a>
            </section>
            <br><hr/>
        @endforeach
    @endif

@endsection