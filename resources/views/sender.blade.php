@extends('layouts.app')

@section('content')
    <h3>Contact Form Send - Laravel</h3>
        <label class="form-inline" for="name">Name:</label>
        <p class="form-inline" id="name" name="fname">{{$name}}</p>
        <label class="form-inline" for="email">Email:</label>
        <p class="form-inline" id="email" name="femail">{{$email}}</p>
        <label class="form-inline" for="text">Texto:</label>
        <p class="form-inline" id="text" name="ftext">{{$text}}</p>
@endsection