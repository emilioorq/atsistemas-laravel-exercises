@extends('layouts.app')

@section('content')
    @if(count($posts)==0)
        <p>Todavia no se ha escrito nada. Sé el primero en escribir una entrada.</p>
    @else
        @foreach($posts as $post)
            @component('components.preview', ['title'=>$post->title,'text'=>$post->text])
            @endcomponent
        @endforeach
    @endif

@endsection