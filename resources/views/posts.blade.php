@extends('layouts.app')

@section('content')
    <h3>Blog Posts - Laravel</h3>
    <hr/>
    @foreach($posts as $post)
        <section>
            <h3>{{$post['title']}}</h3>
            <p>{{$post['text']}}</p>
            <br>
            <a type="button" class="btn btn-info" href="{{route('preview',['title'=>$post['title'], 'text'=>$post['text']])}}">Preview</a>
        </section>
        <br><hr/>
    @endforeach


@endsection
