<li class="nav-item">
    <a class="nav-link" href="{{ route('home') }}">Inicio</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ route('posts') }}">Entradas</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ route('preview') }}">BD Posts</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ route('contact') }}">Contacto</a>
</li>
