<div class="preview">
    <div class="card">
        <div class="card-header">{{$title}} - Laravel</div>

        <div class="card-body">
            <section>
                <h3>{{$title}}</h3>
                <p>{{$text}}</p>
            </section>
            <button href="/" type="button" class="btn btn-info" value="Ok">Leer más</button>
        </div>
    </div>
</div>
<br>