<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CommentsTableSedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create('App\Comment');

        for($i = 1 ; $i <= 10 ; $i++) {
            DB::table('comments')->insert([
                'post_id' => $faker->randomDigitNotNull,
                'email' => $faker->email,
                'comment' => $faker->paragraph(),
                'isResponse' => $faker->boolean,
                'comment_id' => $faker->randomDigitNotNull,
                'created_at' => $faker->datetime(),
                'updated_at' => $faker->datetime()
            ]);
        }
    }
}
