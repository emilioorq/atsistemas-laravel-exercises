<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PostsTableSedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create('App\Post');

        for($i = 1 ; $i <= 10 ; $i++) {
            DB::table('posts')->insert([
                'title' => $faker->sentence(),
                'article' => $faker->paragraph(),
                'created_at' => $faker->datetime(),
                'updated_at' => $faker->datetime()
            ]);
        }
    }
}
