<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Factory as Faker;


class BlogController extends Controller
{
    //
    public function index()
    {
        //return view('home');
    }

    public function show()
    {
        $posts = array();
        $faker = Faker::create();
        for ($i=0;$i<=5;$i++){
            $post =
                [
                  'title' => $faker->name,
                  'text' => $faker->text
                ];
            array_push($posts, $post);
        }
        //return view('posts', ['title'=>'Lista de Entradas del Blog', 'text'=>'Lista de Entradas del Blog']);
        //return view('posts', ['title'=>$faker->name, 'text'=>$faker->text]);
        return view('posts', ['posts'=>$posts]);
    }

    public function preview(string $title, string $text)
    {
        return view('postpreview', ['title'=>$title, 'text'=>$text]);
    }

}
