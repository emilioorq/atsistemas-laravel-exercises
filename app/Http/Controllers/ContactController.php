<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    //
    public function index()
    {
        //return view('home');
    }

    public function form()
    {
        return view('contact');
    }

    public function sender(Request $request)
    {
        $name = $request->input('fname');
        $email = $request->input('femail');
        $text = $request->input('fmessage');
        return view('sender', ['name'=>$name, 'email'=>$email, 'text'=>$text]);
    }

}
