<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/posts', 'BlogController@show')->name('posts');

Route::get('/post/preview/{title}/{text}', 'BlogController@preview')->name('preview');


Route::get('/contact', 'ContactController@form')->name('contact');

Route::post('contact/sender', 'ContactController@sender')->name('sender');

Route::get('/postpreview', 'PostController@index')->name('preview');
